Indicaciones generales:

Para revisar este repositorio lo puede clonar, si lo hace en su computadora personal despues de clonar puede correr en una terminal preferible de git bash el comando npm install y luego ng serve.

Si lo hace desde una maquina virtual como gitpod, el mismo recurso se encargará de instalar los paquetes NPM (la carpeta node_modules); por lo tanto, puede correr en la terminal npm run start. 